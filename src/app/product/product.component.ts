import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  constructor() {}
  products = [
    {
      productName: 'Apple',
      price: 250
    },
    {
      productName: 'Orange',
      price: 300
    },
    {
      productName: 'Banana',
      price: 50
    },
    {
      productName: 'Stroberry',
      price: 350
    },
    {
      productName: 'Watermellon',
      price: 30
    }
  ];

  ngOnInit() {}
}
