import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProductDetailsService {
  productsList: any[] = [
    {
      productName: 'Apple',
      price: 250
    },
    {
      productName: 'Orange',
      price: 300
    },
    {
      productName: 'Banana',
      price: 50
    },
    {
      productName: 'Stroberry',
      price: 350
    },
    {
      productName: 'Watermellon',
      price: 30
    }
  ];
  constructor() {}

  getProducts(_productName: string) {
    // tslint:disable-next-line: max-line-length
    const filteredProducts = _productName ? this.productsList.filter(productsList => productsList.productName === _productName) : this.productsList;
    return filteredProducts;
  }
}
